#include <iostream>
#include <sstream>
#include <fstream>
#include "string"
#include "Knapsack.h"
#include <time.h>

using namespace std;


int conv_str_num(string a)
{
    istringstream convert(a);
    int res;
    if ( !(convert >> res) )
        res = 0;
    return res;
}

int main()
{
    char cadena[128];
    string cad;
    ifstream archivo("knapsack1.txt");
    int cap_lim,cant,i;
    int *valores,*pesos;

    try{
        archivo.getline(cadena, 128);
        cad=cadena;
        i=cad.find(' ');
        cap_lim=conv_str_num(cad.substr(0,i));
        cant=conv_str_num(cad.substr(i+1));
        valores=new int[cant],
        pesos=new int[cant];
        for (int j=0;j<cant;j++)
        {
            archivo.getline(cadena, 128);
            cad=cadena;
            i=cad.find(' ');
            valores[j]=conv_str_num(cad.substr(0,i));
            pesos[j]=conv_str_num(cad.substr(i+1));
        }
        archivo.close();


    }catch(int e){
        return false;
        archivo.close();
    }

/* Instanciando variables*/
        time_t start_time, end_time;
        Knapsack<int> k;
        int valor;

        start_time = time( NULL );

            valor = k.run(valores,pesos,cap_lim,cant);

        end_time = time( NULL );

        printf( "Tiempo demora del algoritmo de la Mochila es: %f seconds\n",
        difftime( end_time, start_time ) );
        cout<<difftime( end_time, start_time ) ;
        cout<<"Valor maximo en la mochila es :"<<valor<<endl;



    return 0;
}
