#ifndef KNAPSACK_H
#define KNAPSACK_H


using namespace std;

template<typename V>
class Knapsack
{
    public:
        Knapsack() {}
        virtual ~Knapsack() {}

        V run(V valores[], int pesos[], int p_lim, int n)
        {
            V *lista1= new V[p_lim+1];
            V *lista2= new V[p_lim+1];
            V a,b;
            bool key=1;
            for (int i=0;i<=p_lim;i++)
                lista1[i]=0;
            for (int i=1;i<=n;i++)
            {
                for (int w=0;w<=p_lim;w++)
                {
                    if (key)
                    {
                        if (pesos[i-1]>w)
                            lista2[w]=lista1[w];
                        else{
                            a=lista1[w];
                            b=lista1[w-pesos[i-1]] + valores[i-1];
                            lista2[w]=max(a,b);
                        }
                    }
                    else
                    {
                        if (pesos[i-1]>w)
                            lista1[w]=lista2[w];
                        else{
                            a=lista2[w];
                            b=lista2[w-pesos[i-1]] + valores[i-1];
                            lista1[w]=max(a,b);
                        }
                    }
                }
                if (key) key=0;
                else key=1;
            }
            V r;
            if (key)
                r=lista1[p_lim];
            else
                r=lista2[p_lim];
            delete []lista2;
            delete []lista1;
            return r;

        }

    protected:
    private:
};

#endif // KNAPSACK_H
